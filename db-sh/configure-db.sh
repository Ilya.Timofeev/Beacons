#!/bin/bash
set -e

psql -h localhost -U postgres <<-EOSQL
CREATE USER sa WITH PASSWORD 'PPaFb9N8';
CREATE DATABASE webapp ENCODING 'UTF8';
GRANT ALL PRIVILEGES ON DATABASE webapp TO sa;
\connect webapp;
CREATE TABLE testdata (id int not null,foo varchar(25),bar int);
INSERT INTO testdata VALUES (1, 'hello', 12345);
GRANT ALL ON TABLE testdata TO sa;
EOSQL
