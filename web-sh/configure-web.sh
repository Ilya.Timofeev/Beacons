#!/bin/bash

version_tomcat="8.5.20"

# Install system packages:
yum install -y java-1.7.0-openjdk-devel java-1.8.0-openjdk-devel wget nc vim

# Install Apache Tomcat:	
groupadd tomcat
useradd -M -s /bin/nologin -g tomcat -d /opt/tomcat tomcat

cd ~
wget "http://apache-mirror.rbc.ru/pub/apache/tomcat/tomcat-8/v$version_tomcat/bin/apache-tomcat-$version_tomcat.tar.gz"
tar xvf apache-tomcat-$version_tomcat.tar.gz -C /opt/tomcat --strip-components=1

cd /opt/tomcat/lib
wget https://jdbc.postgresql.org/download/postgresql-42.1.4.jar

cp -n /opt/tomcat/temp/*store /opt/tomcat/conf
cp -n /opt/tomcat/temp/*.service /etc/systemd/system
yes | cp /opt/tomcat/temp/*.xml /opt/tomcat/conf

cd /opt/tomcat
chgrp -R tomcat /opt/tomcat && chmod -R g+r conf && chown -R tomcat webapps/ work/ temp/ logs/ conf/

systemctl daemon-reload
systemctl start tomcat
systemctl enable tomcat
